package com.sunbeaminfo.sh.pizza.main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.sunbeaminfo.sh.pizza.entities.Item;
import com.sunbeaminfo.sh.pizza.entities.NonVegItem;
import com.sunbeaminfo.sh.pizza.entities.VegItem;
import com.sunbeaminfo.sh.pizza.utils.HbUtil;

public class Hb07Main {
	public static void main(String[] args) {
		try {
			HbUtil.beginTransaction();
			String hql = "from Item i";
			Session session = HbUtil.getSession();
			Query<Item> q = session.createQuery(hql);
			List<Item> list = q.getResultList();
			for (Item i : list) {
				if(i instanceof VegItem)
					System.out.println("Veg" + i);
				else if(i instanceof NonVegItem)
					System.out.println("NonVeg" + i);
			}
			HbUtil.commitTransaction();
		} catch (Exception e) {
			HbUtil.rollbackTransaction();
			e.printStackTrace();
		}
		HbUtil.shutdown();
	}
}
