package com.sunbeaminfo.sh.pizza.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("NonVeg")
public class NonVegItem extends Item {
	//empty
}
